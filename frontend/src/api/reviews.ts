import { handleUnsuccessfulResponse } from "../utils";
import { baseUrl } from "./config";

export type Review = {
  id: number;
  label: number;
  reviewer: string;
  revision: number;
  created_at: Date;
};

export async function createReview(
  revisionId: number,
  labelId: number
): Promise<Review> {
  const data = { revision: revisionId, label: labelId };
  const response = await fetch(`${baseUrl}/reviews`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(data),
  });
  if (!response.ok) {
    handleUnsuccessfulResponse(response);
  }
  return await response.json();
}

export async function updateReview(reviewId: number, labelId: number): Promise<Review> {
  const data = { label: labelId };
  const response = await fetch(`${baseUrl}/reviews/${reviewId}`, {
    method: "PATCH",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(data),
  });
  if (!response.ok) {
    handleUnsuccessfulResponse(response);
  }
  return await response.json();
}
