import { handleUnsuccessfulResponse } from "../utils";
import { baseUrl } from "./config";

export type Project = {
  id: number;
  name: string;
  description: string;
  model: string;
  instructions: string;
  show_model_result: boolean;
  created_at: Date;
  updated_at: Date;
};

export type PaginatedProjects = {
  has_more: boolean;
  projects: Array<Project>;
};

export async function getProject(projectId: number): Promise<Project> {
  const response = await fetch(`${baseUrl}/projects/${projectId}`);
  if (!response.ok) {
    handleUnsuccessfulResponse(response);
  }
  return await response.json();
}

export async function listProjects(
  starting_after?: number,
  limit = 10
): Promise<PaginatedProjects> {
  const params: { [param: string]: string } = { limit: limit.toString() };
  if (starting_after !== undefined) {
    params["starting_after"] = starting_after.toString();
  }
  const queryParams = new URLSearchParams(params);
  const response = await fetch(`${baseUrl}/projects?` + queryParams);
  if (!response.ok) {
    handleUnsuccessfulResponse(response);
  }
  return await response.json();
}
