import { handleUnsuccessfulResponse } from "../utils";
import { baseUrl } from "./config";

export type Revision = {
  id: number;
  wiki_db: string;
  rev: number;
  project: number;
  prediction: string;
  probability: number;
};

export type PaginatedRevisions = {
  has_more: boolean;
  revisions: Array<Revision>;
};

export async function listRevisions(
  projectId: number,
  wikiDb?: string,
  startingAfter?: number,
  limit = 10
): Promise<PaginatedRevisions> {
  const params: { [param: string]: string } = { limit: limit.toString() };
  if (wikiDb !== undefined) {
    params["wiki_db"] = wikiDb;
  }
  if (startingAfter !== undefined) {
    params["starting_after"] = startingAfter.toString();
  }
  const queryParams = new URLSearchParams(params);
  const response = await fetch(
    `${baseUrl}/projects/${projectId}/revisions?` + queryParams
  );
  if (!response.ok) {
    handleUnsuccessfulResponse(response);
  }
  return await response.json();
}
