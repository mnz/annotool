import { handleUnsuccessfulResponse } from "../utils";
import { baseUrl } from "./config";

export enum Role {
  Admin = "admin",
  User = "user",
}

export type User = {
  username: string;
  role: Role;
};

export async function getLoggedInUser(): Promise<User> {
  const response = await fetch(`${baseUrl}/user`);
  if (!response.ok) {
    handleUnsuccessfulResponse(response);
  }
  return await response.json();
}
