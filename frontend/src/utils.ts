export function handleUnsuccessfulResponse(response: Response) {
  throw Error(
    "Failed to load resource. Server responded with: " +
      `${response.status} (${response.statusText})`
  );
}

export function wikiToDomain(wikiDb: string): string {
  switch (wikiDb) {
    case "wikidata":
      return "www.wikidata.org";
    default:
      return `${wikiDb}.wikipedia.org`;
  }
}

export function absolutifyLinks(document: Document, baseUrl: string) {
  for (const link of document.links) {
    const href = link.getAttribute("href");
    if (href !== null && href.startsWith("/") && !href.startsWith("//")) {
      link.href = baseUrl + href;
    }
  }
}
