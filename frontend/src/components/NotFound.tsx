import { Box, Typography } from "@mui/material";

export default function NotFound(): JSX.Element {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        height: `calc(100vh - 64px)`,
      }}
    >
      <Typography variant="h4">404</Typography>
      <Typography variant="subtitle1">page not found</Typography>
    </Box>
  );
}
