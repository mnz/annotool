import { SvgIconComponent } from "@mui/icons-material";
import MemorySharpIcon from "@mui/icons-material/MemorySharp";
import { Box, Button, Card, Stack, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import { Project } from "../api/projects";

type ProjectIconProps = {
  Icon: SvgIconComponent;
  label: string;
};

function ProjectIcon({ Icon, label }: ProjectIconProps): JSX.Element {
  return (
    <Box display="flex" gap={0.5} alignItems={"center"}>
      <Icon color="primary" />
      <Typography variant="body2">{label}</Typography>
    </Box>
  );
}

type Props = {
  project: Project;
};

export default function ProjectCard(props: Props): JSX.Element {
  const { project } = props;
  return (
    <Card elevation={0} sx={{ border: 1, borderColor: "primary.main" }}>
      <Box sx={{ padding: 4 }}>
        <Typography variant="h6" fontWeight={400} color="primary" mb={0.5}>
          {project.name}
        </Typography>
        <Typography variant="body1" color="text.secondary">
          {project.description}
        </Typography>
        <Box
          display="flex"
          mt={2}
          alignItems={"center"}
          justifyContent={"space-between"}
        >
          <Stack direction="row" spacing={2} alignItems={"center"}>
            <ProjectIcon Icon={MemorySharpIcon} label={project.model} />
          </Stack>
          <Button variant="text" component={Link} to={`projects/${project.id}`}>
            Annotate
          </Button>
        </Box>
      </Box>
    </Card>
  );
}
