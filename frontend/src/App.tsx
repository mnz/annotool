import { Navigate, Outlet, Route, Routes } from "react-router-dom";
import Header from "./components/Header";
import NotFound from "./components/NotFound";
import ProjectPanel from "./components/ProjectPanel";
import RevisionPanel from "./components/RevisionPanel";
import { useUser } from "./queries";
import { User } from "./api/user";
import Login from "./components/Login";

function App(): JSX.Element {
  const { data: user, isLoading: isUserLoading } = useUser();

  const ProtectedRoute = ({ user }: { user?: User }): JSX.Element => {
    return user ? <Outlet /> : <Navigate to="/login" replace />;
  };

  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<ProjectPanel />}></Route>
        <Route path="/login" element={<Login />} />
        <Route element={isUserLoading ? <></> : <ProtectedRoute user={user} />}>
          <Route path="/projects/:projectId" element={<RevisionPanel />} />
        </Route>
        <Route path="*" element={<NotFound />}></Route>
      </Routes>
    </>
  );
}

export default App;
