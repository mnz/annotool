from http import HTTPStatus

from flask.testing import FlaskClient
from flask_login import login_user

from annotool import model
from annotool.database import db


base_url = "/api/reviews"


def test_create_review(
    client: FlaskClient,
    example_user: model.User,
    example_revision: model.Revision,
    example_label: model.Label,
) -> None:
    body = {"revision": example_revision.id, "label": example_label.id}
    with client.application.test_request_context():
        login_user(example_user)
        response = client.post(base_url, json=body)
        assert response.status_code == HTTPStatus.OK
        created_review = response.json
        assert created_review is not None
        assert created_review["revision"] == body["revision"]


def test_update_own_review(
    client: FlaskClient,
    example_user: model.User,
    example_revision: model.Revision,
    example_label: model.Label,
) -> None:
    review = model.Review(
        revision=example_revision.id,
        label=example_label.id,
        reviewer=example_user.username,
    )
    db.session.add(review)
    db.session.commit()

    new_label_id = 5

    with client.application.test_request_context():
        login_user(example_user)
        response = client.patch(f"{base_url}/{review.id}", json={"label": new_label_id})
        assert response.status_code == HTTPStatus.OK
        updated_review = response.json
        assert updated_review is not None
        assert updated_review["label"] == new_label_id
        assert updated_review["label"] != example_label.id


def test_update_others_review(
    client: FlaskClient,
    example_user: model.User,
    example_revision: model.Revision,
    example_label: model.Label,
) -> None:
    review = model.Review(
        revision=example_revision.id,
        label=example_label.id,
        reviewer="OtherUser",
    )
    db.session.add(review)
    db.session.commit()

    with client.application.test_request_context():
        login_user(example_user)
        response = client.patch(f"{base_url}/{review.id}", json={"label": 5})
        assert response.status_code == HTTPStatus.NOT_FOUND


def test_list_reviews_no_filter(
    client: FlaskClient, example_revision: model.Revision, example_label: model.Label
) -> None:
    reviews = (
        model.Review(
            revision=example_revision.id,
            label=example_label.id,
            reviewer="RegularUser",
        ),
        model.Review(
            revision=example_revision.id,
            label=example_label.id,
            reviewer="RegularUser",
        ),
    )
    db.session.add_all(reviews)
    db.session.commit()

    response = client.get(base_url)
    assert response.status_code == HTTPStatus.OK
    review_list = response.json
    assert review_list is not None
    assert len(review_list["reviews"]) > 1


def test_filter_reviews_by_project(
    client: FlaskClient,
    example_project: model.Project,
    example_revision: model.Revision,
    example_label: model.Label,
) -> None:
    reviews = (
        model.Review(
            revision=example_revision.id,
            label=example_label.id,
            reviewer="RegularUser",
        ),
        model.Review(
            revision=example_revision.id,
            label=example_label.id,
            reviewer="RegularUser",
        ),
    )
    db.session.add_all(reviews)
    db.session.commit()

    query = db.select(model.Review).filter(
        model.Review.revision == model.Revision.id,
        model.Revision.project == example_project.id,
    )
    reviews_in_db = db.session.scalars(query).all()

    response = client.get(f"{base_url}?project={example_project.id}")
    assert response.status_code == HTTPStatus.OK
    review_list = response.json
    assert review_list is not None
    assert len(review_list["reviews"]) == len(reviews_in_db)
    review_ids_in_db = set(review.id for review in reviews_in_db)
    review_ids = set(review["id"] for review in review_list["reviews"])
    assert review_ids == review_ids_in_db


def test_summarize_reviews(
    client: FlaskClient,
    example_revision: model.Revision,
    example_label: model.Label,
) -> None:
    reviews = (
        model.Review(
            revision=example_revision.id,
            label=example_label.id,
            reviewer="RegularUser",
        ),
        model.Review(
            revision=example_revision.id,
            label=example_label.id,
            reviewer="RegularUser",
        ),
    )
    db.session.add_all(reviews)
    db.session.commit()

    query = db.select(model.Review).filter(
        model.Review.revision == model.Revision.id,
        model.Review.label == example_label.id,
        model.Revision.project == example_revision.project,
        model.Revision.wiki_db == example_revision.wiki_db,
        model.Revision.rev == example_revision.rev,
    )
    reviews_in_db = db.session.scalars(query).all()

    response = client.get(
        f"{base_url}/summary?"
        f"project={example_revision.project}&"
        f"wiki_db={ example_revision.wiki_db}&"
        f"rev={ example_revision.rev}"
    )
    assert response.status_code == HTTPStatus.OK
    summary = response.json
    assert summary is not None
    assert summary["label_counts"][f"{example_label.id}"] == len(reviews_in_db)
