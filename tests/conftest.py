from typing import Generator

import pytest

from flask import Flask
from flask.testing import FlaskClient
from pydantic import SecretStr

from annotool import create_app, model
from annotool.config import Config
from annotool.database import db


@pytest.fixture(scope="session")
def config() -> Config:
    config = Config(
        DATABASE_URI="sqlite:///:memory:",
        ADMIN_PASSWORD=SecretStr("adminpassword"),
        MEDIAWIKI_OAUTH_KEY="oauthkey",
        MEDIAWIKI_OAUTH_SECRET=SecretStr("oauthsecret"),
        MEDIAWIKI_OAUTH_URL="https://meta.wikimedia.org/w/rest.php",
        SECRET_KEY=SecretStr("secretkey"),
        ADMIN_USERNAMES=["SuperUser"],
    )
    return config


@pytest.fixture(scope="session")
def app(config: Config) -> Flask:
    return create_app(config)


@pytest.fixture(scope="function")
def client(app: Flask) -> Generator[FlaskClient, None, None]:
    with app.app_context():
        # set up
        db.create_all()

        yield app.test_client()

        # tear down
        db.drop_all()


@pytest.fixture(scope="function")
def example_user() -> model.User:
    return model.User("RegularUser")


@pytest.fixture(scope="function")
def example_admin() -> model.User:
    return model.User("SuperUser")


@pytest.fixture(scope="function")
def example_project() -> model.Project:
    project = model.Project(
        name="Test Project",
        description="Test Description.",
        model="Test Model",
        instructions="Test instructions",
        show_model_result=False,
    )
    db.session.add(project)
    db.session.commit()
    return project


@pytest.fixture(scope="function")
def example_revision(example_project: model.Project) -> model.Revision:
    revision = model.Revision(
        project=example_project.id,
        rev=12345,
        wiki_db="abwiki",
        prediction="reverted",
        probability=0.75,
    )
    db.session.add(revision)
    db.session.commit()
    return revision


@pytest.fixture(scope="function")
def example_label(example_project: model.Project) -> model.Label:
    label = model.Label(
        name="good",
        project=example_project.id,
    )
    db.session.add(label)
    db.session.commit()
    return label


@pytest.fixture(scope="function")
def example_review(
    example_revision: model.Revision,
    example_label: model.Label,
    example_user: model.User,
) -> model.Review:
    review = model.Review(
        revision=example_revision.id,
        label=example_label.id,
        reviewer=example_user.username,
    )
    db.session.add(review)
    db.session.commit()
    return review
