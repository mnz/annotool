from http import HTTPStatus
from pathlib import Path
from typing import Optional

import flask
import spectree

from authlib.integrations.flask_client import OAuth
from flask_login import LoginManager
from werkzeug.exceptions import HTTPException

from annotool import model
from annotool.config import Config
from annotool.database import db, init_db_command
from annotool.utils.error_handlers import handle_http_exception
from annotool.utils.json import CustomJSONProvider


oauth = OAuth()

login_manager = LoginManager()

spec = spectree.SpecTree(
    "flask",
    path="docs",
    annotations=True,
    mode="strict",
    title="Annotool API",
    validation_error_status=HTTPStatus.BAD_REQUEST,
    security_schemes=[
        spectree.SecurityScheme(
            name="auth_CookieAuth",
            data=spectree.SecuritySchemeData.parse_obj(
                {"type": "apiKey", "in": "cookie", "name": "session"}
            ),
        ),
    ],
)


@login_manager.user_loader
def load_user(user_id: str) -> model.User:
    return model.User(username=user_id)


def create_app(config: Optional[Config] = None) -> flask.Flask:
    """Create and configure an instance of the Flask application."""

    app = flask.Flask(__name__)

    if config is None:
        config = Config.from_yaml(
            root_path=Path(__file__).resolve().parent.parent,
            filename="config.yaml",
        )

    app.config["SECRET_KEY"] = config.SECRET_KEY.get_secret_value()
    app.config["MAX_CONTENT_LENGTH"] = 5 * 100 * 1000
    app.config["ADMINS"] = config.ADMIN_USERNAMES
    app.config["SQLALCHEMY_DATABASE_URI"] = config.DATABASE_URI

    oauth.init_app(app)
    oauth.register(
        name="mediawiki",
        api_base_url=f"{config.MEDIAWIKI_OAUTH_URL}/",
        client_id=config.MEDIAWIKI_OAUTH_KEY,
        client_secret=config.MEDIAWIKI_OAUTH_SECRET.get_secret_value(),
        access_token_url=f"{config.MEDIAWIKI_OAUTH_URL}/oauth2/access_token",
        authorize_url=f"{config.MEDIAWIKI_OAUTH_URL}/oauth2/authorize",
        fetch_token=lambda: flask.session.get("token"),
    )

    login_manager.init_app(app)

    db.init_app(app)
    app.cli.add_command(init_db_command)

    app.json = CustomJSONProvider(app)
    app.register_error_handler(HTTPException, handle_http_exception)

    from annotool.api import api
    from annotool.auth import auth
    from annotool.client import client

    spec.register(api)
    spec.register(auth)

    app.register_blueprint(api)
    app.register_blueprint(client)
    app.register_blueprint(auth)

    return app
