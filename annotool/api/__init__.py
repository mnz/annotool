from flask import Blueprint

from annotool.api import labels, projects, reviews, revisions, user


api = Blueprint("api", __name__, url_prefix="/api")
api.register_blueprint(projects.bp, url_prefix="/projects")
api.register_blueprint(labels.bp, url_prefix="/projects/<int:project_id>/labels")
api.register_blueprint(revisions.bp, url_prefix="/projects/<int:project_id>/revisions")
api.register_blueprint(reviews.bp, url_prefix="/reviews")
api.register_blueprint(user.bp, url_prefix="/user")
