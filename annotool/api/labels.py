import spectree

from flask import Blueprint
from werkzeug.exceptions import NotFound

from annotool import model, schema, spec
from annotool.database import db
from annotool.utils.auth import admin_login_required


bp: Blueprint = Blueprint("labels", __name__)


@bp.route("", methods=["POST"])
@spec.validate(
    resp=spectree.Response(HTTP_200=schema.Label),
    security={"auth_CookieAuth": []},
)
@admin_login_required
def create_label(project_id: int, json: schema.LabelCreate) -> schema.Label:
    if model.Project.get(id=project_id) is None:
        raise NotFound(f"Could not find project with id: {project_id}")

    label = model.Label(name=json.name, project=project_id)
    db.session.add(label)
    db.session.commit()
    return schema.Label.from_orm(label)


@bp.route("")
@spec.validate(resp=spectree.Response(HTTP_200=schema.LabelList))
def list_labels(project_id: int, query: schema.LabelPaginate) -> schema.LabelList:
    if model.Project.get(id=project_id) is None:
        raise NotFound(f"Could not find project with id: {project_id}")

    stmt = db.select(model.Label).filter_by(project=project_id)
    results = model.Label.paginate(
        selectable=stmt,
        limit=query.limit,
        starting_after_id=query.starting_after,
    )
    labels = [label for label, *_ in results.data]
    return schema.LabelList(labels=labels, has_more=results.has_more)
