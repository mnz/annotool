import datetime

from enum import Enum, unique

import flask

from flask_login import UserMixin
from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    Float,
    ForeignKey,
    Integer,
    String,
    Text,
)

from annotool.database import BaseModel


utc_now = datetime.datetime.utcnow


class Project(BaseModel):
    id = Column(Integer, primary_key=True)
    name = Column(String(length=255))
    description = Column(Text)
    model = Column(String(length=255))
    instructions = Column(Text)
    show_model_result = Column(Boolean, default=True)
    created_at = Column(DateTime(timezone=True), default=utc_now)
    updated_at = Column(DateTime(timezone=True), default=utc_now, onupdate=utc_now)


class Label(BaseModel):
    id = Column(Integer, primary_key=True)
    name = Column(String(length=255))
    project = Column(Integer, ForeignKey(Project.id, ondelete="CASCADE"))


class Revision(BaseModel):
    id = Column(Integer, primary_key=True)
    rev = Column(Integer)
    wiki_db = Column(String(length=255))
    project = Column(Integer, ForeignKey(Project.id, ondelete="CASCADE"))
    prediction = Column(String(length=255))
    probability = Column(Float)


class Review(BaseModel):
    id = Column(Integer, primary_key=True)
    revision = Column(Integer, ForeignKey(Revision.id, ondelete="CASCADE"))
    label = Column(Integer, ForeignKey(Label.id, ondelete="CASCADE"))
    reviewer = Column(String(length=255), nullable=False)
    created_at = Column(DateTime(timezone=True), default=utc_now)


@unique
class Role(str, Enum):
    ADMIN = "admin"
    USER = "user"


class User(UserMixin):  # type: ignore
    def __init__(self, username: str):
        self.username = username

    @property
    def role(self) -> Role:
        admins = flask.current_app.config["ADMINS"]
        return Role.ADMIN if self.username in admins else Role.USER

    def get_id(self) -> str:
        return self.username
