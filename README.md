# Annotool API

API for Annotool on Toolforge.

## Development

### Prerequisites

* [Poetry](https://python-poetry.org/docs/#installation)
* [NPM](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)
* A database backend; [SQLite](https://www.sqlite.org/index.html) is included, but you may use any database supported by [SQLAlchemy](https://www.sqlalchemy.org/features.html).

### Setup

After installing the prerequisites, run:

```bash
git clone https://gitlab.wikimedia.org/repos/mnz/annotool.git
cd annotool
poetry shell
```
#### Configuration

Add your `config.yaml` (see [config.template.yaml](config.template.yaml)).

#### App Installation

```bash
poetry install --directory=annotool --no-root
npm --prefix frontend install
npm --prefix frontend run build
```

#### Initialize database and run

```bash
flask init-db
flask run
```

### Testing

To run all tests and exit to a poetry shell:

```bash
poetry run pytest -vv
```

### Hooks

To set up the pre-commit hooks, run:

```bash
pre-commit install
```

This will lint and typecheck the code on every commit. Hooks and tests are also run in the CI.

### API Usage

After logging in with an account in the `ADMIN_USERNAME` list, you will be authorized to perform all actions in the API. You may use the builtin Swagger UI "try it out" feature as a convenient way to interact with the API. see [http://localhost:5000/api/docs/swagger/](http://localhost:5000/api/docs/swagger/).
